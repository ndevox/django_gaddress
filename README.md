# Django Addresses

This is a simplistic library for validating and storing locations
using the Google Places API.

## Dependencies and setup

This library is a Geographical one and relies on `django.contrib.gis`
being setup. You can do this by following the guide 
[here](https://docs.djangoproject.com/en/2.1/ref/contrib/gis/).

Once done add the following to your `INSTALLED_APPS` setting:

    INSTALLED_APPS += [
        'django.contrib.gis',
        
        'djangorestframework',  # Not 100% necessary but recommended.
        'django_gaddress',
    ]

Lastly run migrations:

    python manage.py migrate


## Usage

The views expose two endpoints `/address/query` and `/address/create`.

The query endpoint takes anything that could be used as a place identifier
for example a post code or point of interest. It will query the places API
and return a list of possible options for the user to choose from.

The create endpoint takes a place identifier, runs a query to the places
API and uses the details of the first returned address to store in the DB.

It is recommended to present a query first to the user and use a choice for
the create call. This way it is more likely to get an accurate address.

## Warning

Google has T&Cs around the use of their APIs including the storing of 
Places data. Data can be stored to improve UX (at the time or writing)
but needs to be updated regularly from Google.

This is also not production ready in any way and is in active development.
Breaking changes are likely.

## Testing and Development

Requirements for development are in `dev-requirements.txt`.

This project uses pytest and tox. You can test by running:

    pytest .

or

    tox
