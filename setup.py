from setuptools import setup

install_reqs = [req.strip() for req in open('requirements.txt').readlines()]
test_reqs = [req.strip() for req in open('dev-requirements.txt').readlines()[1:]]

setup(
    name="django_gaddress",
    version="0.3.0",
    packages=['django_gaddress', 'django_gaddress.contrib.wagtail'],
    install_requires=install_reqs,
    setup_requires=['pytest-runner'],
    tests_require=test_reqs,
    extras_require={
        'wagtail': ['wagtail'],
    },
    url="https://gitlab.com/ndevox/django_gaddress"
)
