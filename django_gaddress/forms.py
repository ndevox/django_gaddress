import uuid

from django.contrib.gis import forms

from django_gaddress import models, utils
from django_gaddress.utils import get_client


SESSION_TOKEN = uuid.uuid4().hex


class NewAddressForm(forms.ModelForm):
    loc = forms.PointField()

    class Meta:
        model = models.Address
        fields = '__all__'


class CreateAddressForm(forms.Form):
    address = forms.CharField(max_length=1024)

    def execute(self):
        data = utils.get_address_details(self.cleaned_data)

        form = NewAddressForm(data)

        if not form.is_valid():
            return False, form.errors
        return True, form.save()


class AddressQueryForm(forms.Form):
    query = forms.CharField(max_length=512)

    def execute(self):
        client = get_client()
        return client.places_autocomplete(self.cleaned_data['query'], SESSION_TOKEN)
