from rest_framework import serializers

from django_gaddress import models


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        exclude = ['created', 'last_updated']
