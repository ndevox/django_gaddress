import googlemaps
from django.conf import settings
from django.contrib.gis.geos import Point


def get_client():
    if hasattr(settings, 'GADDRESS_API_KEY'):
        return googlemaps.Client(key=settings.GADDRESS_API_KEY)
    return googlemaps.Client()


def get_address_details(address):
    keys = {
        'street_number',
        'route',
        'postal_town',
        'locality',
        'administrative_area_level_1',
        'administrative_area_level_2',
        'country',
        'postal_code'
    }

    update_dict = {}

    client = get_client()

    details = client.geocode(address)[0]

    # This is the tricky bit we have to very repetitively update.
    for item in details['address_components']:
        matching_keys = set(item['types']) & keys
        # It is unlikely, but we could have multiple matching keys.
        # To deal with this we can use a while loop and pop.
        while matching_keys:
            update_dict[matching_keys.pop()] = item['long_name']

    update_dict['formatted_address'] = details['formatted_address']

    coords = details['geometry']['location']

    update_dict['lat'], update_dict['lng'] = coords['lat'], coords['lng']
    update_dict['loc'] = Point(update_dict['lng'], update_dict['lat'])

    return update_dict
