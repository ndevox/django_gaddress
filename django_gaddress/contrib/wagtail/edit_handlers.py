from django.conf import settings
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from wagtail.admin.edit_handlers import InlinePanel, widget_with_script


class InlineMapFieldPanel(InlinePanel):

    template = 'django_gaddress/edit_handlers/inline_map_panel.html'
    js_template = 'django_gaddress/edit_handlers/inline_map_panel.js'

    def __init__(self, *args, **kwargs):
        self.default_centre = kwargs.pop('centre', getattr(settings, 'GADDRESS_MAP_CENTER', None))
        self.zoom = kwargs.pop('zoom', getattr(settings, 'GADDRESS_MAP_ZOOM', 8))
        super().__init__(*args, **kwargs)

    def clone(self):
        instance = super().clone()

        instance.default_centre = self.default_centre
        instance.zoom = self.zoom
        return instance

    def classes(self):
        classes = super().classes()
        classes.append('wagtailgmap')
        return classes

    def render(self, *args, **kwargs):
        maps_api_js = 'https://maps.googleapis.com/maps/api/js?key={}'.format(settings.GADDRESS_API_KEY)
        language = getattr(settings, 'GADDRESS_MAP_LANGUAGE', None)
        if language:
            maps_api_js += '&language={}'.format(language)

        formset = render_to_string(self.template, {
            'self': self,
            'can_order': self.formset.can_order,
            'maps_js_url': maps_api_js,
        })
        js = self.render_js_init()
        return widget_with_script(formset, js)

    def render_js_init(self):
        return mark_safe(render_to_string(self.js_template, {
            'self': self,
            'can_order': self.formset.can_order,
            'zoom': self.zoom,
            'address': settings.GADDRESS_MAP_CENTER,
        }))
