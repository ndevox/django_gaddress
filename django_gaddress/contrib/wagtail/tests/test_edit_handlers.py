from unittest import mock

from django_gaddress.contrib.wagtail.edit_handlers import InlineMapFieldPanel


def test_inline_map_field_panel_clone():
    panel = InlineMapFieldPanel('address', zoom=1, centre=2)
    new_panel = panel.clone()

    assert new_panel.zoom == panel.zoom
    assert new_panel.default_centre == panel.default_centre


def test_inline_map_field_classes():
    panel = InlineMapFieldPanel('address')
    assert 'wagtailgmap' in panel.classes()


def test_inline_map_field_renders():
    panel = InlineMapFieldPanel('address')

    panel.formset = mock.MagicMock()
    panel.render()
