{% load l10n %}
{% load wagtailadmin_tags %}

var geocoder = new google.maps.Geocoder();

function setUpMap(prefix, defaultPosition){

    function formatId(id) {
        return '#id_' + prefix + '-' + id;
    }

    // Create map options and map
    var mapOptions = {
        zoom: {{ zoom }},
        center: defaultPosition,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = {
        map: new google.maps.Map(document.getElementById(prefix + '_map'), mapOptions),
        input: $('#' + prefix + '_search')
    };

    map.marker = new google.maps.Marker({
      position: defaultPosition,
      map: map.map,
      draggable: true
    });

    function setValues(data) {
        var fields = [
            'street_number',
            'route',
            'postal_town',
            'locality',
            'administrative_area_level_1',
            'administrative_area_level_2',
            'country',
            'postal_code'
        ];

        fields.forEach(function(field) {
            var realField = $(formatId(field));
            realField.val('');
            for (var i = 0; i < data.address_components.length; i++) {
                if (data.address_components[i].types.includes(field)) {
                    realField.val(data.address_components[i].long_name);
                    return
                }
            }
        });

        $(formatId('formatted_address')).val(data.formatted_address);
        $(formatId('lat')).val(data.geometry.location.lat());
        $(formatId('lng')).val(data.geometry.location.lng());
    }

    function geocodePosition(pos) {
        geocoder.geocode({
          latLng: pos
        }, function(responses) {
          if (responses && responses.length > 0) {
              setValues(responses[0]);
          } else {
            alert('Cannot determine address at this location.');
          }
        });
    }

    // Get LatLong position and formatted address from inaccurate address string
    function geocodeAddress(address, marker, map) {
        geocoder.geocode({'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            marker.setPosition(results[0].geometry.location);
            setValues(results[0]);
            map.setCenter(results[0].geometry.location);
          } else {
            alert("Geocode was not successful for the following reason: " + status);
          }
        })
    }

    // Set events listeners to update marker/input values/positions
    google.maps.event.addListener(map.marker, 'dragend', function(event) {
      geocodePosition(map.marker.getPosition());
    });
    google.maps.event.addListener(map.map, 'click', function(event) {
      map.marker.setPosition(event.latLng);
      geocodePosition(map.marker.getPosition());
    });

    // Event listeners to update map when press enter or tab
    $(map.input).bind("enterKey",function(event) {
        event.preventDefault();
      geocodeAddress($(this).val(), map.marker, map.map);
    });

    $(map.input).keypress(function(event) {
        if(event.keyCode == 13)
        {
          event.preventDefault();
          $(this).trigger("enterKey");
        }
    });

    google.maps.event.trigger(map.map, 'resize');


}

(function() {

    var panel = InlinePanel({
        formsetPrefix: "id_{{ self.formset.prefix }}",
        emptyChildFormPrefix: "{{ self.empty_child.form.prefix }}",
        canOrder: {% if can_order %}true{% else %}false{% endif %},
        maxForms: {{ self.formset.max_num|unlocalize }}
    });

    {% for child in self.children %}
        panel.initChildControls("{{ child.form.prefix }}");
        geocoder.geocode({"address": "{{ address }}"}, function(results, status){
            if (status === google.maps.GeocoderStatus.OK) {
                setUpMap("{{ child.form.prefix }}", results[0].geometry.location);
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    {% endfor %}
    panel.setHasContent();
    panel.updateMoveButtonDisabledStates();
    panel.updateAddButtonState();
})();