from django.conf.urls import include, url

from rest_framework import routers

from django_gaddress import views


address_router = routers.SimpleRouter()
address_router.register(r'address', views.AddressViewSet, basename='address')

urlpatterns = [
    url(r'^', include(address_router.urls)),
]
