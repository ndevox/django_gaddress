from unittest import mock

from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.test import TestCase

from rest_framework.test import APIClient

from django_gaddress import models


class AddressViewsTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

        user = User.objects.create(
            username='user',
            password='password'
        )

        self.client.force_authenticate(user)

        self.data = {
            'route': 'Test Address',
            'country': 'Test Country',
            'postal_code': 'WC1A 1AA',
            'formatted_address': 'Test Address, Test Country, WC1A 1AA',
            'lat': 1.0,
            'lng': 2.0,
            'loc': Point(2.0, 1.0),
        }

    @mock.patch('googlemaps.Client')
    def test_query(self, mock_client):
        mock_autocomplete = mock.MagicMock()
        mock_autocomplete.places_autocomplete.return_value = 'hello'

        mock_client.return_value = mock_autocomplete

        resp = self.client.get('/address/query/', data={'query': 'test'})

        assert resp.status_code == 200
        assert mock_autocomplete.places_autocomplete.call_count == 1
        assert resp.json() == 'hello'

    def test_failed_query(self):

        resp = self.client.get('/address/query/')

        assert resp.status_code == 400
        assert resp.json() == {'query': ['This field is required.']}

    @mock.patch('django_gaddress.utils.get_address_details')
    def test_create(self, mock_details):
        mock_details.return_value = self.data

        resp = self.client.post('/address/', {'address': 'test'})

        assert resp.status_code == 201
        assert mock_details.call_count == 1
        assert models.Address.objects.count() == 1

    @mock.patch('django_gaddress.utils.get_address_details')
    def test_create_invalid_form(self, mock_details):
        self.data.pop('route')
        mock_details.return_value = self.data

        resp = self.client.post('/address/', {'address': 'test'})

        assert resp.status_code == 400
        assert mock_details.call_count == 1
        assert resp.json() == {'route': ['This field is required.']}

    def test_create_no_address(self):

        resp = self.client.post('/address/', {})

        assert resp.status_code == 400
        assert resp.json() == {'address': ['This field is required.']}
