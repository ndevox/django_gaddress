import factory
from django.contrib.gis.geos import Point

from django_gaddress import models


class AddressFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Address

    route = 'Test Address'
    country = 'Test Country'
    postal_code = 'WC1A 1AA'
    formatted_address = 'Test Address, Test Country, WC1A 1AA'
    lng = 1.0
    lat = 1.0
    loc = Point(lng, lat)
