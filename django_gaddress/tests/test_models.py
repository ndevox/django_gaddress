from unittest import mock

from django.contrib.gis.geos import Point
from django.test import TestCase
import pytest

from django_gaddress import models
from django_gaddress.tests import factories


@pytest.mark.django_db
class AddressTestCase(TestCase):
    def setUp(self):
        self.data = {
            'route': 'Test Address',
            'country': 'Test Country',
            'postal_code': 'WC1A 1AA',
            'formatted_address': 'Test Address, Test Country, WC1A 1AA',
            'lat': 1.0,
            'lng': 2.0
        }

        self.address1 = factories.AddressFactory(formatted_address='closest')
        self.address2 = factories.AddressFactory(formatted_address='close')
        self.address2.loc = Point(1.0, 1.5)
        self.address2.save()
        self.address3 = factories.AddressFactory(formatted_address='far', lat=50.0, lng=50.0)
        self.address3.save()

    def test_full_address(self):
        assert self.address1.formatted_address == self.address1.full_address

    def test_str(self):
        assert str(self.address1) == self.address1.full_address

    def test_coordinates(self):
        assert self.address1.coordinates == (1.0, 1.0)

    @mock.patch('django_gaddress.utils.get_address_details')
    def test_update_location(self, mock_get_ad_details):

        self.data['route'] = 'Test Address 2'

        mock_get_ad_details.return_value = self.data

        self.address1.update_location()
        self.address1.refresh_from_db()

        assert self.address1.route == 'Test Address 2'
        assert self.address1.last_updated != self.address1.created

    def test_within_filter(self):
        query = models.Address.objects.within(Point(1.0, 1.0), 100000)

        assert query.count() == 2
        assert not query.filter(pk=self.address3.pk)
        assert query.filter(pk__in=(self.address2.pk, self.address1.pk)).count() == 2

    def test_by_distance(self):
        query = models.Address.objects.by_distance(Point(1.0, 1.0, srid=4326))
        pks = {self.address1.pk, self.address2.pk, self.address3.pk}
        assert pks == {address.pk for address in query}

    def test_by_distance_reverse(self):
        query = models.Address.objects.by_distance(
            Point(1.0, 1.0, srid=4326),
            reverse=True
        )
        pks = {self.address3.pk, self.address2.pk, self.address1.pk}
        assert pks == {address.pk for address in query}

    def test_by_distance_filter(self):
        query = models.Address.objects.by_distance(
            Point(1.0, 1.0, srid=4326),
            formatted_address=self.address3.formatted_address
        )
        pks = {self.address3.pk}
        assert pks == {address.pk for address in query}
