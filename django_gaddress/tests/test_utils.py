from unittest import mock

from django_gaddress import utils


@mock.patch('googlemaps.Client')
def test_get_address_details(mock_client):

    mock_geocoder = mock.MagicMock()

    mock_geocoder.geocode.return_value = [{
        'address_components': [{'types': ['route'], 'long_name': 'test street'}],
        'formatted_address': 'A nice formatted address',
        'geometry': {'location': {'lat': 1.0, 'lng': 2.0}}
    }]

    mock_client.return_value = mock_geocoder

    res = utils.get_address_details('test address')

    assert res['lat'] == 1.0
    assert res['lng'] == 2.0
    assert res['formatted_address'] == 'A nice formatted address'
    assert res['route'] == 'test street'
