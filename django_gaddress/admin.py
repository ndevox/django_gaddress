from django.contrib import admin

from django_gaddress import models


class AddressAdmin(admin.ModelAdmin):
    readonly_fields = ('lat', 'lng')


admin.site.register(models.Address, AddressAdmin)
