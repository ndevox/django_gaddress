from rest_framework import decorators
from rest_framework import viewsets
from rest_framework.response import Response

from django_gaddress import serializers, models, forms


class AddressViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post']
    serializer_class = serializers.AddressSerializer

    queryset = models.Address.objects.all()

    @decorators.action(methods=['get'], detail=False)
    def query(self, request):
        form = forms.AddressQueryForm(request.GET)

        if form.is_valid():
            return Response(form.execute())

        return Response(form.errors, status=400)

    def create(self, request, *args, **kwargs):

        create_form = forms.CreateAddressForm(request.data)

        if not create_form.is_valid():
            return Response(create_form.errors, status=400)

        success, errors = create_form.execute()
        if not success:
            return Response(errors, status=400)

        return Response(status=201)
