"""
Models for holding address based information.
"""

from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.utils import timezone

from django_gaddress import utils


class AddressManager(models.Manager):
    def within(self, point: Point, distance: int) -> models.QuerySet:
        """
        Return a set of results within a restricted distance.

        :param point: The point from which we are measuring from.
        :param distance: The maximum distance we will return results from in
        metres.
        :return: A queryset filtered on distance.
        """
        return self.filter(loc__dwithin=(point, distance))

    def by_distance(self, point: Point, reverse: bool = False, **kwargs) -> models.QuerySet:
        """
        Return a QuerySet sorted by distance.
        """
        key = 'distance'
        if reverse:
            key = '-' + key
        return self.annotate(distance=Distance('loc', point)).filter(**kwargs).order_by(key)


class Address(models.Model):
    """ Address Model based off of data from Google Maps API. """

    objects = AddressManager()

    street_number = models.CharField(max_length=128, null=True, blank=True)
    route = models.CharField(max_length=128)

    postal_town = models.CharField(max_length=128, null=True, blank=True)
    locality = models.CharField(max_length=128, null=True, blank=True)

    administrative_area_level_1 = models.CharField(max_length=64, null=True, blank=True)
    administrative_area_level_2 = models.CharField(max_length=64, null=True, blank=True)

    country = models.CharField(max_length=32)

    postal_code = models.CharField(max_length=8)

    formatted_address = models.CharField(max_length=512)

    lat = models.FloatField()
    lng = models.FloatField()

    loc = models.PointField()

    created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.loc = Point(self.lng, self.lat)

        return super(Address, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields
        )

    def update_location(self):
        """
        Use the GeoCoding API from Google to analyse a location.

        This returns a JSON object as detailed here:
        https://developers.google.com/maps/documentation/geocoding/intro#GeocodingResponses

        We break these down into the important components (listed in the keys var).

        The relevant fields are updated and the object is saved.

        Google TOS enforce that we update our data at least every 30 days.

        This method allows us to easily keep compliance with this.
        """

        data = utils.get_address_details(self.formatted_address)

        data['last_updated'] = timezone.now()
        Address.objects.filter(pk=self.pk).update(**data)

    @property
    def full_address(self):
        """
        Use the descriptive address given by Google.

        :return: A descriptive full address.
        :rtype: str
        """
        return self.formatted_address

    @property
    def coordinates(self):
        """
        Return the co-ordinates of the address.

        If the co-ordinates have not yet been found, analyse the address.

        :return: A tuple containing the co-ordinates (lt, lng)
        :rtype: tuple(float, float)
        """
        return self.lat, self.lng

    def __str__(self):
        return self.full_address
